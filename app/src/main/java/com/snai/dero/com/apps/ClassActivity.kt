package com.snai.dero.com.apps

import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.snai.dero.com.apps.utils.Utils
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ClassActivity : AppCompatActivity() {
    companion object {
        const val FILECHOOSER_RESULTCODE = 10
    }

    private lateinit var webView: WebView
    private var mFilePathCallback: ValueCallback<Array<Uri>>? = null
    private lateinit var cameraUri: Uri
    private lateinit var dialogTitle: String


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogTitle = getString(R.string.dialog_title)
        setContentView(R.layout.activity_class)
        checkPermissions()
        cameraUri = Uri.EMPTY
        webView = findViewById(R.id.web)
        val cookieManager = CookieManager.getInstance()
        CookieManager.setAcceptFileSchemeCookies(true)
        cookieManager.setAcceptThirdPartyCookies(webView, true)
        webView.settings.defaultTextEncodingName = "utf-8"
        webView.settings.useWideViewPort = true
        webView.settings.builtInZoomControls = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.displayZoomControls = false
        webView.settings.allowFileAccess = true
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                var url = ""
                if (request != null && request.url != null) {
                    url = request.url.toString()
                }
                if (!url.startsWith(BuildConfig.HTTP)) {
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    return true
                }
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                url?.let {
                    Utils.saveCurrentUrl(this@ClassActivity, it)
                }
            }
        }
        webView.webChromeClient = object : WebChromeClient() {
            override fun onShowFileChooser(
                webView: WebView?,
                filePathCallback: ValueCallback<Array<Uri>>?,
                fileChooserParams: FileChooserParams?
            ): Boolean {
                checkPermissions()
                mFilePathCallback = filePathCallback
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent.resolveActivity(packageManager) != null) {
                    var photoFile: File? = null
                    try {
                        photoFile = createImageFile()
                    } catch (ex: IOException) {
                        Log.e("TAG", "Unable to create Image File", ex)
                    }

                    if (photoFile != null) {
                        val photoURI = FileProvider.getUriForFile(
                            this@ClassActivity,
                            this@ClassActivity.applicationContext.packageName.toString() + ".provider",
                            photoFile
                        )
                        cameraUri = photoURI
                        takePictureIntent.putExtra(
                            MediaStore.EXTRA_OUTPUT,
                            photoURI
                        )
                    }
                }
                takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                contentSelectionIntent.type = "image/*"
                val intentArray: Array<Intent?> = arrayOf(contentSelectionIntent)
                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                chooserIntent.putExtra(Intent.EXTRA_INTENT, takePictureIntent)
                chooserIntent.putExtra(Intent.EXTRA_TITLE, dialogTitle)
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE)
                return true
            }
        }
        webView.loadUrl(Utils.getCurrentUrl(this))
    }

    private fun checkPermissions() {
        Dexter.withContext(this).withPermissions(WRITE_EXTERNAL_STORAGE, CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    // also nothing to do
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    // nothing to  do
                }


            }).check()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "JPEG_max_" + timeStamp + "_"
        val storageDir =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) getExternalFilesDir("")
            else getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        intent: Intent?
    ) {
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mFilePathCallback) return
            val result = if (intent == null || resultCode != RESULT_OK) null else intent.data
            if (result != null) {
                mFilePathCallback?.let {
                    intent?.let { it1 ->
                        it.onReceiveValue(arrayOf(Uri.parse(it1.data.toString())))
                    }
                }
            } else {
                mFilePathCallback?.onReceiveValue(arrayOf(cameraUri))
            }

        }
        mFilePathCallback = null
        super.onActivityResult(requestCode, resultCode, intent)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            webView.loadUrl(Utils.getInitialUrl(this))
        }
    }
}
