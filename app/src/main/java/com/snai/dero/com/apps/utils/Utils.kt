package com.snai.dero.com.apps.utils

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.snai.dero.com.apps.BuildConfig

object Utils {
    const val CURRENT_URL_NAME = "CURRENT_URL"
    const val INIT_URL_NAME = "INIT_URL"
    const val FIREBASE_URL_NAME = "url"
    const val SP_NAME = "SP"

    fun initFirebaseRemote(activity: AppCompatActivity, callback: Runnable){
        val firebaseConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(1000)
            .build()
        firebaseConfig.setConfigSettingsAsync(configSettings)
        firebaseConfig.fetchAndActivate().addOnCompleteListener(activity) {
            var initUrl = firebaseConfig.getString(FIREBASE_URL_NAME)
            if(initUrl.isEmpty()){
                initUrl = BuildConfig.BINOM
            }
            activity.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).edit().putString(
                INIT_URL_NAME, initUrl).apply()
            callback.run()
        }
    }

    fun getInitialUrl(activity: AppCompatActivity): String{
        return activity.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(INIT_URL_NAME, BuildConfig.BINOM).orEmpty()
    }

    fun saveCurrentUrl(context: Context, currentUrl: String){
        context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).edit().putString(
            CURRENT_URL_NAME, currentUrl).apply()
    }

    fun getCurrentUrl(activity: AppCompatActivity): String{
        return activity.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(
            CURRENT_URL_NAME, getInitialUrl(activity)).orEmpty()
    }
}