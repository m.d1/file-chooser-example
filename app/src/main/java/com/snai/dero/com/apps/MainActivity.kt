package com.snai.dero.com.apps

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.snai.dero.com.apps.utils.Utils

class MainActivity : AppCompatActivity() {
    private var initializationComplete = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(getSharedPreferences(Utils.SP_NAME, Context.MODE_PRIVATE).contains(Utils.CURRENT_URL_NAME)){
            initializationComplete = true
        } else {
            Utils.initFirebaseRemote(this) {
                initializationComplete = true
            }
        }
    }

    fun onClick(view: View){
        if(initializationComplete){
            startActivity(Intent(this, ClassActivity::class.java))
        }
    }
}